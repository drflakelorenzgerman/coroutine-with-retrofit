package com.sass.coroutinerestsample

import retrofit2.http.GET

interface webService {
    @GET("posts")
    suspend fun getPosts() : MutableList<Post>
}