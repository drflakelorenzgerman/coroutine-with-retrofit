package com.sass.coroutinerestsample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.liveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() , RemoteErrorEmitter  {
    val webService : webService = RetrofitBuilder.api

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var thePosts = apiCall<MutableList<Post>>(this,{webService.getPosts()}).observe(
            this,
            Observer {
                if(it != null)
                    for (item in it)
                    {
                        Log.d("HERE",item.title)
                    }
            }
        )

    }

    override fun onError(msg: String) {
    }

    override fun onError(errorType: ErrorType) {
    }
}
